# Lien vers la presentation

https://f2671.gitlab.io/formation-docker-kube-support/formation-docker-kube-2022


## Lien vers les exercices 

- Premiere seance : https://www.katacoda.com/ragatzino/scenarios/formation-docker-sndio
- Seconde séance : https://gitlab.com/f2671/exercices-kubernetes 
- Troisième séance : https://gitlab.com/f2671/exercices-kubernetes-avec-helm
- Quatrième séance : https://gitlab.com/f2671/atelier-ci-kubernetes
